<?php

namespace Control;

use App\Models\Product;
use Illuminate\Support\Facades\Auth;

class Policy
{
    const actions_for_user = ['add', 'update', 'delete'];

    /**
     * @return object
     * get user
     */
    public static function getUser()
    {
        return Auth::user();
    }

    /**
     * @param bool $admin
     * @return bool
     * check user role
     */
    public static function isAdmin()
    {
        if(self::getUser()->is_admin)
            return true;

        return false;
    }

    /**
     * @param $product_id
     * @return int
     * get user id from product id
     */
    public static function getUserIdFromProduct($product_id)
    {
        return Product::find($product_id)->user_id;
    }

    /**
     * @param $action
     * @param $product_id
     * @return bool
     * check opportunity to action
     */
    public static function denies($product_id, $action = self::actions_for_user)
    {
        if(!self::isAdmin()){
            if(self::getUserIdFromProduct($product_id) != self::getUser()->id){
                return false;
            }
        }

        return true;
    }
}
