<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;
use Control\Policy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $products = Product::all();
        $products->each(function($product){
            if(Policy::denies($product->id)){
                $product->my = true;
            }
            $product->owner = User::find(Policy::getUserIdFromProduct($product->id))->name;
        });
        return view('home', compact('products'));
    }


    public function add(Request $request)
    {
        $valid = $request->validate([
            'name' => 'required|min:1|max:200',
            'price' => 'integer|min:1',
        ]);

        Product::create([
            'user_id' => Auth::id(),
            'name' => $request->name,
            'price' => $request->price,
        ]);

        return redirect()->route('home')->with('Status', 'Успешно добавлен товар');
    }


    public function show($id)
    {
        $product = Product::find($id);
        $product->owner = User::find(Policy::getUserIdFromProduct($id))->name;
        if(Policy::denies($product->id)){
            $product->my = true;
        }
        return view('show', compact('product'));
    }


    public function update($id, Request $request)
    {
        $valid = $request->validate([
            'name' => 'required|min:1|max:200',
            'price' => 'integer|min:1',
        ]);

        if(Policy::denies($id)){
            Product::where('id', $id)->update([
                'name' => $request->name,
                'price' => $request->price,
            ]);
        }

        return redirect()->route('show', ['id' => $id])->with('Status', 'Успешно обновлён товар');
    }


    public function delete($id)
    {
        if(Policy::denies($id)){
            Product::destroy($id);
        }

        return redirect()->route('home');
    }
}
