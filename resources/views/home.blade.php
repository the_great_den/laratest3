@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <table>
                            <tbody>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">Товар</th>
                                <th scope="col">Цена</th>
                                <th scope="col">Владелец</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                            @foreach($products as $product)
                            <tr>
                                <th>{{$product->id}}</th>
                                <th><a href="{{route('show', ['id' => $product->id])}}">{{$product->name}}</a></th>
                                <th>{{$product->price}}</th>
                                <th>{{$product->owner}}</th>
                                <td>@if($product->my)<a href="{{route('show', ['id' => $product->id])}}"><button>Редактировать</button></a>@endif</td>
                                <td>
                                    @if($product->my)
                                        <form method="post" action="{{route('delete', ['id' => $product->id])}}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit">Delete</button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
<form action="{{route('add_product')}}" method="post">
    @csrf
    <ul>
        <li>
            <label for="name">Товар:</label>
            <input type="text" id="name" name="name" required>
        </li>
        <li>
            <label for="mail">Цена:</label>
            <input type="number" id="mail" name="price" required>
        </li>
        <button type="submit">Save</button>
        <a href="">Add</a>
    </ul>
</form>
@endsection
