@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a href="{{route('home')}}">вернутся к товарам</a></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <table>
                            <tbody>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">Товар</th>
                                <th scope="col">Цена</th>
                                <th scope="col">Владелец</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                            <tr>
                                <th>{{$product->id}}</th>
                                <th>{{$product->name}}</th>
                                <th>{{$product->price}}</th>
                                <th>{{$product->owner}}</th>
                            </tr>
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@if($product->my)
<form action="{{route('show', ['id' => $product->id])}}" method="post">
    @csrf
    <ul>
        <li>
            <label for="name">Товар:</label>
            <input type="text" id="name" name="name" value="{{$product->name}}" required>
        </li>
        <li>
            <label for="mail">Цена:</label>
            <input type="number" id="mail" name="price" value="{{$product->price}}" required>
        </li>
        <button type="submit">Save</button>
        <a href="">Edit</a><a href="">Delete</a>
    </ul>
</form>

<form method="post" action="{{route('delete', ['id' => $product->id])}}">
    @csrf
    @method('DELETE')
    <button type="submit">Delete</button>
</form>

    @endif
@endsection
