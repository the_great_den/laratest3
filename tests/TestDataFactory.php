<?php

namespace Tests;

/*use App\Entity\User;
use App\Entity\Product;*/

use App\Models\User;
use App\Models\Product;

class TestDataFactory
{
    public static function createUser(): User
    {
        return User::factory()->create();
    }

    public static function createProduct(User $user): Product
    {
        return Product::factory()->create(['user_id' => $user->id]);
    }

    public static function createAdminUser(): User
    {
        return User::factory()->create([
            'is_admin' => true
        ]);
    }
}
