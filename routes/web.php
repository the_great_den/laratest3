<?php

use App\Http\Controllers\FaceBookController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::prefix('facebook')->name('facebook.')->group( function(){
    Route::get('auth', [FaceBookController::class, 'loginUsingFacebook'])->name('login');
    Route::get('callback', [FaceBookController::class, 'callbackFromFacebook'])->name('callback');
});


Route::get('/products', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/products', [App\Http\Controllers\HomeController::class, 'add'])->name('add_product');

Route::delete('/products/{id}', [App\Http\Controllers\HomeController::class, 'delete'])->where('id', '\d+')->name('delete');
Route::get('/products/{id}', [App\Http\Controllers\HomeController::class, 'show'])->where('id', '\d+')->name('show');
Route::post('/products/{id}', [App\Http\Controllers\HomeController::class, 'update'])->where('id', '\d+')->name('update');

